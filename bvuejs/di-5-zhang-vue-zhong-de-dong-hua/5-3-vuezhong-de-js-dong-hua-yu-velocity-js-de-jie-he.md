### 1.入场动画

1.1@before-enter 动画执行之前

eg:在元素显示时,将文字的颜色变为红色

```js
    <div id="app">
        <transition
        name="fade"
        @before-enter="handleBeforeEnter"
        >
            <div v-if="show">hello world</div>            
        </transition>
        <button @click="handleClick">toggle</button>
    </div>
    <script>
        var app = new Vue({
            el:"#app",
            data:{
                show:true
            },
            methods:{
                handleClick:function(){
                    this.show = !this.show;
                },
                //el指的就是transition包裹的div标签
                handleBeforeEnter:function(el){
                    el.style.color="red"
                }
            }
        })
    </script>
```

1.2@enter 执行动画

@after-enter动画执行完毕

```js
    <div id="app">
        <transition
        name="fade"
        @before-enter="handleBeforeEnter"
        @enter="handleEnter"
        @after-enter="handleAfterEnter"
        >
            <div v-if="show">hello world</div>            
        </transition>
        <button @click="handleClick">toggle</button>
    </div>
    <script>
        var app = new Vue({
            el:"#app",
            data:{
                show:true
            },
            methods:{
                handleClick:function(){
                    this.show = !this.show;
                },
                //el指的就是transition包裹的div标签
                handleBeforeEnter:function(el){
                    el.style.color="red"
                },
                //done回调函数,动画执行完毕调用
                handleEnter:function(el,done){
                    setTimeout(()=>{
                        el.style.color="green"
                    },2000)
                    setTimeout(()=>{
                        done()
                    },4000)
                },
                handleAfterEnter:function(el){
                        el.style.color="#333";
                }
            }
        })
    </script>
```

### 2.出场动画

* leave
* before-leave
* after-leave

### 3.velocity的使用

[http://velocityjs.org/](http://velocityjs.org/)

```
Velocity(document.getElementById("dummy"), { opacity: 0.5 }, { duration: 1000 });
//
Velocity(el,style,time)
```

```js
   <div id="app">
        <!-- 
            @before-enter进入动画之前
            @enter进入动画
            @after-enter 动画执行完
         -->
        <transition
        name="fade"
        @before-enter="handleBeforeEnter"
        @enter="handleEnter"
        @after-enter="handleAfterEnter"
        >
            <div v-if="show">hello world</div>            
        </transition>
        <button @click="handleClick">toggle</button>
    </div>
    <script>
        var app = new Vue({
            el:"#app",
            data:{
                show:true
            },
            methods:{
                handleClick:function(){
                    this.show = !this.show;
                },
                //el指的就是transition包裹的div标签
                handleBeforeEnter:function(el){
                   el.style.opacity = 0;
                },
                //done回调函数,动画执行完毕调用
                handleEnter:function(el,done){
                    Velocity(el,{opacity:1},
                    {duration:1000,
                    complete:done
                    })
                },
                handleAfterEnter:function(el){
                     alert("动画执行完成")   
                }
            }
        })
    </script>
```



