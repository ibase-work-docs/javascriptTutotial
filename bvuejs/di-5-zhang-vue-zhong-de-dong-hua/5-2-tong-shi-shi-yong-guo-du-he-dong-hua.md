1.第一次出现有动画

```js
    <div id="app">
        <transition 
        //加一个appear属性
        appear
        appear-active-class="animated swing"
        enter-active-class="animated bounceInDown" leave-active-class="animated bounceInUp">
            <div v-if="show">hello world</div>
        </transition>
        <button @click="handleClick">toggle</button>
    </div>

    <script>
        var app = new Vue({
            el:"#app",
            data:{
                show:true
            },
            methods:{
                handleClick:function(){
                    this.show = !this.show;
                }
            }
        })
    </script>
```

2.过渡和动画同时出现

```js
//css
        <style>
        .fade-enter{
            opacity: 0;
        }
        .fade-enter-active{
            transition:opacity 1s;
        }
        .fade-leave-active{
            transition:opacity 1s;
        }
        .fade-leave-to{
            opacity: 0;
        }
    </style>

//HTML,JS
    <div id="app">
        <!-- type=transition表示动画时长以 transition为主 -->
        <transition 
        type="transition"
        name="fade"
        appear
        appear-active-class="animated swing"
        enter-active-class="animated swing fade-enter-active" 
        leave-active-class="animated shake fade-leave-active">
            <div v-if="show">hello world</div>
        </transition>
        <button @click="handleClick">toggle</button>
    </div>

    <script>
        var app = new Vue({
            el:"#app",
            data:{
                show:true
            },
            methods:{
                handleClick:function(){
                    this.show = !this.show;
                }
            }
        })
    </script>
```

3.自定义动画的时长:duration=time

//自定义时长为5秒钟

```js
<transition 
        :duration="5000"
        name="fade"
        appear
        appear-active-class="animated swing"
        enter-active-class="animated swing fade-enter-active" 
        leave-active-class="animated shake fade-leave-active">
            <div v-if="show">hello world</div>
</transition>
```



