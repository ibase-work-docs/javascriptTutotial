## 1.列表过渡

* ##### 技术要点:transition-group

```js
    <style>
        .v-enter,.v-leave-to{
            opacity: 0;
        }
        .v-enter-active,.v-leave-active{
            transition: opacity 1s;
        }
    </style>

    <div id="root">
        <transition-group>
            <div v-for="(item,index) of list" :key="item.id">{{item.title}} {{item.id}}</div>
        </transition-group>
        <button @click="handleClick">Add</button>
    </div>
    <script>
        var count=0;
        var app = new Vue({
            el:"#root",
            data:{
                list:[]
            },
            methods:{
                handleClick:function(){
                    this.list.push({id:count++,title:"hello world"})
                }
            }
        })
    </script>
```

//加transition-group,相当于在每个元素外围加了transition

