### 1.多个元素的过渡动画  要给元素加key值

```js
    <style>
        .v-enter,.v-leave-to{
            opacity: 0;
        }
        .v-enter-active,.v-leave-active{
            transition:opacity 1s;
        }
    </style>
    <div id="app">
        <transition >
            <div v-if="show" key="hello">hello</div>
            <div v-else key="world">world</div>
        </transition>
        <button @click="handleClick">toggle</button>
    </div>
    <script>
        var app = new Vue({
            el:"#app",
            data:{
                show:true
            },
            methods:{
                handleClick:function(){
                    this.show= !this.show
                }
            }
        })
    </script>
```

### 2.组件的动画

```js
    <style>
        .v-enter,.v-leave-to{
            opacity:0;
        }
        .v-enter-active,.v-leave-active{
            transition:opacity 1s;
        }
    </style>
    <div id="app">
        <transition>
            <child-one v-if="show"></child-one>
            <chidl-two v-else></chidl-two>
        </transition>
        <button @click="handleClick">toggle</button>
    </div>
    <script>
        Vue.component("child-one",{
            template:'<div>child-one</div>'
        })
        Vue.component("child-two",{
            template:'<div>child-two</div>'
        })
        var app = new Vue({
            el:"#app",
            data:{
                show:true
            },
            methods:{
                handleClick:function(){
                    this.show = !this.show;
                }
            }
        })
    </script>
```

### 3.动态组件

```
//根据传入的type值不同动态决定显示那个组件
<component :is="type"></component>
```

```js
    <style>
        .v-enter,.v-leave-to{
            opacity:0;
        }
        .v-enter-active,.v-leave-active{
            transition:opacity 1s;
        }
    </style>
    <div id="app">
        <transition mode="out-in">
            <component :is="type"></component>
        </transition>
        <button @click="handleClick">toggle</button>
    </div>
    <script>
        Vue.component("child-one",{
            template:'<div>child-one</div>'
        })
        Vue.component("child-two",{
            template:'<div>child-two</div>'
        })
        var app = new Vue({
            el:"#app",
            data:{
                type:"child-one"
            },
            methods:{
                handleClick:function(){
                    this.type =(this.type === "child-one")?"child-two":"child-one"
                }
            }
        })
    </script>
```



