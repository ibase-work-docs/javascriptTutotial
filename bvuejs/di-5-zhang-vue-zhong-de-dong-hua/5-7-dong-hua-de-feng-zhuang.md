### 动画的封装

技术要点

* Vue.component
* 模板中有transiton和slot

1.css样式动画

```
        .v-enter,
        .v-leave-to {
            opacity: 0;
        }

        .v-enter-active,
        .v-leave-active {
            transition: opacity 1s;
        }
```

```js
    <div id="root">
        <fade :show='show'>
            <div>hello world</div>
        </fade>
        <button @click="handleClick">toggle</button>
    </div>
    <script>
        Vue.component('fade', {
            props: ['show'],
            template: `
                <transition>
                    <slot v-if="show"></slot>
                </transition>
            `
        })
        var vm = new Vue({
            el: "#root",
            data: {
                show: false
            },
            methods: {
                handleClick: function () {
                    this.show = !this.show
                }
            }
        })
    </script>
```

2.JS动画样式 封装在模板中

```js
    <div id="root">
        <fade :show='show'>
            <div>hello world</div>
        </fade>
        <button @click="handleClick">toggle</button>
    </div>
    <script>
        Vue.component('fade', {
            props: ['show'],
            template: `
                <transition
                @before-enter="handleBeforeEnter"
                @enter="handleEnter"
                >
                    <slot v-if="show"></slot>
                </transition>
            `,
            methods:{
                handleBeforeEnter:function(el){
                    el.style.color="red"
                },
                handleEnter:function(el,done){
                    setTimeout(()=>{
                        el.style.color="green";
                        done();
                    },2000)
                }
            }
        })
        var vm = new Vue({
            el: "#root",
            data: {
                show: false
            },
            methods: {
                handleClick: function () {
                    this.show = !this.show
                }
            }
        })
    </script>
```



