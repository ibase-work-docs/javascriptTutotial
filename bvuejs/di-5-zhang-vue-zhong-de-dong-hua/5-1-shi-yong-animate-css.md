1.使用预定义的class动画

```
<div id="app">
        <transition name="fade">
            <div v-show="show">hello world</div>
        </transition>

        <button @click="handleClick">切换</button>
    </div>
    <script>
        var app = new Vue({
            el: "#app",
            data: {
                show: true
            },
            methods: {
                handleClick: function () {
                    this.show = !this.show;
                }
            }
        })
</script>
```

```
//css
        @keyframes bounce-in{
            0%{
                transform: scale(0)
            }
            50%{
                transform: scale(1.5)
            }
            100%{
                transform: (1)
            }
        }
        .fade-enter-active{
            transform-origin:left center;
            animation:bounce-in 1s;
        }
        .fade-leave-active{
            transform-origin: left center;
            animation:bounce-in 1s reverse;
        }
```

2.自定义class的动画

```html
<transition name="fade" enter-active-class="active" leave-active-class="leave">
            <div v-show="show">hello world</div>
</transition>
```

```
        //css
        .active{
            transform-origin:left center;
            animation:bounce-in 1s;
        }
        .leave{
            transform-origin: left center;
            animation:bounce-in 1s reverse;
        }
```

3.使用animated.css库

[https://daneden.github.io/animate.css/](https://daneden.github.io/animate.css/)

```css
<transition name="fade" enter-active-class="animated swing" leave-active-class="animated shake">
            <div v-show="show">hello world</div>
</transition>
```



