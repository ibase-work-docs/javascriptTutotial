```js
    <div id="app">
        <table>
            <tbody>
                 <tr is="row"></tr>
                 <tr is="row"></tr>
                 <tr is="row"></tr>
            </tbody>
        </table>
    </div>

    <script>
        Vue.component('row', {
            template: '<tr><td>this is row</td></tr>'
        })
        var app = new Vue({
            el: "#app"
        })
    </script>
```

//使用is属性 解决表格的小bug

