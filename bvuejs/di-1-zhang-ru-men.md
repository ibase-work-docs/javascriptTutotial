```
<div id="root">{{msg}}</div>
```

```
//实例
new Vue({
    //挂载点
    el:"#root",
    data:{
        msg:"hello world"
    }
})
```

模板指定

```
v-text/html  数据渲染
@click 事件绑定
:title 属性绑定
v-model input双向数据绑定
```

1-1 挂载点,模板，实例的关系

1-2 实例中的数据渲染,事件和方法

1-3 属性绑定和双向数据绑定

1-4 计算属性和侦听器

1-5 v-if,v-show,v-for

在线实例

[https://gitee.com/frontendLol/vueTutorial/tree/master/01入门](https://gitee.com/frontendLol/vueTutorial/tree/master/01入门)

