```js
    
    <div id="root">
            <child @click.native="handleClick"></child>        
    </div>
    <script>
        Vue.component("child",{
            template:"<div >child</div>"
        })
        var app = new Vue({
            el:"#root",
            methods:{
                handleClick:function(){
                    alert(1)
                }
            }
        })
    </script>
```

```
//给组件加原生事件,要在事件后面加.native
<child @click.native="handleClick"></child>
```



