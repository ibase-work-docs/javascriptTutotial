//组件参数校验

组件接收的参数,是有规则的

props特性,指父组件向子组件传递一个属性,子组件有对应的属性

1.子组件接收父组件传递的参数必须是String

```js
    <div id="app">
        <child :content="123"></child>
    </div>
    <script>
        Vue.component("child",{
        //期望父组件传递的是字符串
            props:{
                content:String
            },
            template:"<div>{{content}}</div>"

        })
        var app = new Vue({
```

```
//传递的要么是数字,要么是字符串
content:[Number,String]
```

```js
        props:{
                content:{
                    type:String,
                    //true表示必须传递content,false表示不传递
                    required:true,
                    //default默认值
                    default:'default value'
                }
        },
```

//通过校验器,校验传入的参数的长度大于5

```js
props:{
    content:{
        type:String,
        required:true,
        default:'default value',
        validator:function(value){
            return (value.length>5)
        }
    }
}
```

2.非props特性



