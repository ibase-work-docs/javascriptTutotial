```js
<div id="app">
        <child-one v-if="type === 'child-one'"></child-one>
        <child-two v-if="type === 'child-two'"></child-two>
        <button @click="handleChange">change</button>
    </div>
    <script>
        Vue.component("child-one", {
            template: "<div>child-one</div>"
        })
        Vue.component("child-two", {
            template: "<div>child-two</div>"
        })
        var app = new Vue({
            el: "#app",
            data: {
                type: "child-one"
            },
            methods: {
                handleChange: function () {
                    this.type = (this.type === 'child-one') ? "child-two" : "child-one"
                }
            }
        })
    </script>
```

可以简写为动态组件 [:is](/:is)根据传入的type值不同,显示那个组件

```js
    <div id="app">
        <component :is="type"></component>
        <button @click="handleChange">change</button>
    </div>
    <script>
        Vue.component("child-one", {
            template: "<div v-once>child-one</div>"
        })
        Vue.component("child-two", {
            template: "<div v-once>child-two</div>"
        })
        var app = new Vue({
            el: "#app",
            data: {
                type: "child-one"
            },
            methods: {
                handleChange: function () {
                    this.type = (this.type === 'child-one') ? "child-two" : "child-one"
                }
            }
        })
    </script>
```

v-once将静态的DOM存入内存中,重新展示的时候节省性能

