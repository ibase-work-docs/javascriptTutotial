非父子组件之间的传值

```
Bus/总线/发布订阅模式/观察者模式
```

观察者模式（Observer）：通常又被称作为发布-订阅者模式。它定义了一种一对多的依赖关系，即当一个对象的状态发生改变的时候，所有依赖于它的对象都会得到通知并自动更新，解决了主体对象与观察者之间功能的耦合。

```js
    <!-- 点击Dell,Lee变成Dell -->
    <div id="root">
        <child content="Dell"></child>
        <child content="Lee"></child>
    </div>
    <script>
        // 在Vue类挂载bus这个属性
        Vue.prototype.bus=new Vue();
        Vue.component("child",{
            props:{
                content:String
            },
            data:function(){
                return {
                    childContent:this.content
                }
            },
            template:"<div @click='handleClick'>{{childContent}}</div>",
            methods:{
                handleClick:function(){
                    // 触发事件
                    this.bus.$emit('change',this.childContent)
                }
            },
            mounted:function(){
               var this_=this;
               //事件的监听
                this.bus.$on('change',function(msg){
                    this_.childContent=msg
                })
            }
        })
        var vm=new Vue({
            el:"#root"
        })
    </script>
```

```
1.给类增加一个bus属性
2.this.bus.$emit触发事件
3.mounted中 this.bus.$on接收事件
```



