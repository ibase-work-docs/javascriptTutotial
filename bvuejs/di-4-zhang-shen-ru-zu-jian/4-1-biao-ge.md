Tip：在子组件中,data是函数

```js
    <div id="app">
        <table>
            <tbody>
                 <tr is="row"></tr>
                 <tr is="row"></tr>
                 <tr is="row"></tr>
            </tbody>
        </table>
    </div>

    <script>
        // 在子组件中,data是函数
        Vue.component('row', {
            data:function(){
                return {
                    content:"this is row"
                }
            },
            template: '<tr><td>{{content}}</td></tr>'
        })
        // 在根组件中,data是对象
        var app = new Vue({
            el: "#app"
        })
```



