```js
    <!-- ref引用操作dom -->
    <div id="app">
        <div ref="hello" @click="handleClick">hello world</div>
    </div>
    <script>
        var app = new Vue({
            el:"#app",
            methods:{
                handleClick:function(){
                    alert(this.$refs.hello.innerHTML)
                }
            }
        })
    </script>
```

eg:

```
<div ref="test"></div>

this.$ref.test 可以获取这个DOM
```

## 1.点击子组件counter让它的number加一

```js
        <div id="app">
            <counter  ref="one"></counter>
            <counter  ref="two"></counter>
        </div>

        // 点击子组件加一
        //总数
        Vue.component("counter",{
            data: function(){
                return {
                    number : 0
                }
            },
            template:"<div @click='handleClick'>{{number}}</div>",
            methods:{
                handleClick:function(){
                    this.number++;
                }
            }
        })
        var app = new Vue({
            el:"#app"
        })
```

### 2.对子组件的值求和

```js
    <div id="app">
        <counter @change="handleChange" ref="one"></counter>
        <counter @change="handleChange" ref="two"></counter>
        <div>{{total}}</div>
    </div>

    <script>
        // 点击子组件加一
        //总数
        Vue.component("counter",{
            data: function(){
                return {
                    number : 0
                }
            },
            template:"<div @click='handleClick'>{{number}}</div>",
            methods:{
                handleClick:function(){
                    this.number++;
                    this.$emit("change");
                }
            }
        })
        var app = new Vue({
            el:"#app",
            data:{
                total:0
            },
            methods:{
                handleChange:function(){
                    this.total = this.$refs.one.number+this.$refs.two.number;
                }
            }
        })
```

https://gitee.com/frontendLol/vueTutorial/tree/master/03%E6%B7%B1%E5%85%A5%E7%BB%84%E4%BB%B6

