### 1.单向数据流-父组件向子组件传递数据

单向数据流,父组件可以随意的向子组件传递参数,子组件不可以随意改变父组件传递的参数，如果修改了其他子组件使用这个数据,也会受到影响

eg:在子组件中直接改变父组件传递的参数

```js
     <div id="app">
        <counter :content="count"></counter>
    </div>
    <script>
        var counter = {
            props: ['content'],
            template: "<div @click='handleClick'>{{content}}</div>",
            methods: {
                handleClick: function () {
                    this.content++;
                }
            }
        }
        var app = new Vue({
            el: "#app",
            components: {
                counter: counter

            },
            data: {
                count: 0
            }
        })
    </script>
```

控制台会报错,提示不要直接修改父组件传递的参数

![](/assets/console.png)

可以使用data属性来接收,父组件传递的参数,修改代码如下

```js
    <div id="app">
        <counter :content="count"></counter>
    </div>
    <script>
        // 单向数据流,父组件可以随意的向子组件传递参数,子组件不可以随意改变父组件传递的参数
        var counter = {
            props: ['content'],
            template: "<div @click='handleClick'>{{number}}</div>",
            data:function(){
                return{number:this.content}
            },
            methods: {
                handleClick: function () {
                    this.number++;
                }
            }
        }
        var app = new Vue({
            el: "#app",
            components: {
                counter: counter

            },
            data: {
                count: 0
            }
        })
    </script>
```

Tip:父组件通过,属性的形式向子组件传值,子组件通过事件触发的形式向父组件传值。

