插槽  向插槽中插入内容

父组件优雅的向子组件传递DOM结构

```js
    <!-- 父组件向子组件优雅的传递DOM结构 -->
    <div id="app">
        <child>
            <!-- 向子组件插入内容 -->
            <p>Dell</p>
        </child>
    </div>
    <script>
        Vue.component("child", {
            props: ['content'],
            template: `<div>
                            <p>hello</p>
                            <slot>默认内容</slot>
                        </div>`
        });
        var app = new Vue({
            el: "#app"
        })
    </script>
```

控制台中显示结果为

```
<div>
    <p>hello</p>
    <p>Dell</p>
</div>
```

#### 具名插槽

```js
<!-- 父组件向子组件优雅的传递DOM结构 -->
    <div id="app">
        <child>
            <!-- 向子组件插入内容 -->
            <div class="header" slot="header">header</div>
            <div class="footer" slot="footer">footer</div>
        </child>
    </div>
    <script>
        // slot标签显示的是所有插槽的内容
        Vue.component("child", {
            template: `<div>
                            <slot name="header"></slot>
                            <div class='content'>content</div>
                            <slot name="footer"></slot>                            
                        </div>`
        });
        var app = new Vue({
            el: "#app"
        })
    </script>
```



