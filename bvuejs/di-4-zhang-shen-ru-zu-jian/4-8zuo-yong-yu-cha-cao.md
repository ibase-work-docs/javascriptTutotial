作用域插槽

```js
    <div id="app">
        <child>
            <!-- 作用域插槽
                slot-scope父组件从子组件接收的数据放在这个属性中
             -->
            <template slot-scope="props">
                <li>{{props.item}}</li>
            </template>
        </child>
    </div>
    <script>
        Vue.component("child",{
            data:function(){
                return {
                    list:[1,2,3,4]
                }
            },
            template:`<div>
                        <ul>
                            <slot v-for="item of list" :item=item></slot>
                        </ul>
                      </div>`
        })
        var vm = new Vue({
            el:"#app"
        })
    </script>
```



