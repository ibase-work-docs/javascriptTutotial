使用@click  v-model v-for

eg: 点击按钮,将输入框的内容,添加到列表

```js
    <!-- 将输入框的数据显示在列表中 -->
    <div id="root">
        <div>
            <input type="text" v-model="inputValue">
            <button @click="add">提交</button>
        </div>
        <ul>
            <li v-for="(item,index) of list" :key="index">{{item}}</li>
        </ul>
    </div>
    <script>
        new Vue({
            el:"#root",
            data:{
                inputValue:"hello",
                list:[]
            },
            methods:{
                add :function(){
                    this.list.push(this.inputValue);
                    this.inputValue="";
                    }
            }
        })
    </script>
```



