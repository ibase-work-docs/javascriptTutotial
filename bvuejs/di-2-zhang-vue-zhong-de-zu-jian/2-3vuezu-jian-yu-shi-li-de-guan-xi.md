每一个vue的组件就是一个实例

eg:给组件添加一个click事件

```js
    <div id="root">
        <input type="text" v-model="inputValue">
        <button @click="add">提交</button>
        <ul>
            <todo-list v-for="item of list" :content="item">
            </todolist>
        </ul>

    </div>
    <script>
        Vue.component("todo-list",{
            props:['content'],
            template:"<li @click='handleClick'>{{content}}</li>",
            methods:{
                handleClick:function(){
                    alert(123)
                }
            }
        })
        new Vue({
            el:"#root",
            data:{
                inputValue:"hello",
                list:[]
            },
            methods: {
                add : function(){
                    this.list.push(this.inputValue)
                }
            }
        })
    </script>
```

vue实例如果不定义template\(模板\),那么它会将挂载点下所有的标签,作用实例的模板;

