1.Vue.component\(\) 全局组件

* tempalte

* props

```
//语法Vue.component(componentName,{template:html})
//全局组件
Vue.component("todo-item",{
    template:<h1>hello world</h1>
})
```

```js
     <div id="root">
        <input type="text" v-model="inputValue">
        <button @click="add">提交</button>
        <ul>
            <todo-item></todo-item>
        </ul>
    </div>

    <script>
        全局组件
        Vue.component('todo-item',{
            template:'<li>item</li>'
        })

        new Vue({
            el:"#root",
            data:{
                inputValue:"hello",
                list:[]
            },
            methods:{
                add : function(){
                    this.list.push(this.inputValue)
                }
            }
        })
    </script>
```

2.局部组件

```js
        //局部组件
        var TodoItem = {
            template:"<li>item</li>"
        }
        //components对局部组件进行注册
        components:{
        "todo-item":TodoItem
        }
```

```js
    <div id="root">
        <input type="text" v-model="inputValue">
        <button @click="add">提交</button>
        <ul>
            <todo-item></todo-item>
        </ul>
    </div>  
    <script>
        //局部组件
        var TodoItem = {
            template:"<li>item</li>"
        }
        new Vue({
            el:"#root",
            //components对局部组件进行注册
            components:{
                "todo-item":TodoItem
            },
            data:{
                inputValue:"hello",
                list:[]
            },
            methods:{
                add : function(){
                    this.list.push(this.inputValue)
                }
            }
        })
    </script>
```



