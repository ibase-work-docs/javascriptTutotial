```js
    <div id="root">
        <input type="text" v-model="inputValue">
        <button @click="add">提交</button>
        <ul>
            <todo-item v-for="(item,index) of list" :key="index"
            //运用props属性和data之间通信
            :content='item'></todo-item>
        </ul>
    </div>

    <script>
        // 全局组件
        Vue.component('todo-item',{
            //接收从外部传递进来的content属性
            props:['content'],
            template:'<li>{{content}}</li>'
        })

        new Vue({
            el:"#root",
            data:{
                inputValue:"hello",
                list:[]
            },
            methods:{
                add : function(){
                    this.list.push(this.inputValue)
                }
            }
        })
    </script>
```

组件运用props这个属性和实例中的data通信

