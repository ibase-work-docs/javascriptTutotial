// 在vue中父组件向子组件传值,是通过属性的形式

```js
    <!-- 点击子组件,删除 -->
    <div id="root">
        <input type="text" v-model="inputValue">
        <button @click="add">提交</button>
        <ul>
            <!-- @delete父组件监听子组件的delte函数 -->
            <todo-list v-for="(item,index) of list" 
            :content="item"
            :key="index"
            :index="index"
            @delete="handleDelete"
            >
            </todolist>
        </ul>

    </div>
    <script>
        // 在vue中父组件向子组件传值,是通过属性的形式
        Vue.component("todo-list",{
            props:['content','index'],
            template:"<li @click='handleClick'>{{content}}{{index}}</li>",
            methods:{
                handleClick:function(){

                    //想外触发一个delete事件
                    this.$emit('delete',this.index)
                }
            }
        })
        new Vue({
            el:"#root",
            data:{
                inputValue:"hello",
                list:[]
            },
            methods: {
                add : function(){
                    this.list.push(this.inputValue)
                },
                //子组件传递一个参数index
                handleDelete : function(index){
                    this.list.splice(index,1)
                }
            }
        })
```

1.父组件通过属性的形式向子组件传递参数

2.子组件this.$emit\("delete",this.index\)  子组件通过发布一个事件,父组件定义这个事件,子组件就可以向父组件传递数据

