安装环境

* node
* githun
* vue-cli

### 1.路由

概念:就是根据网页的不同,返回不同的内容给用户

```
//显示的是当前路由地址所对应的内容
<router-view/>
```

如何使用

* 设置组件-在views的文件中设置
* 在router.js文件中注册
* 在main.js中引入router.js
* 在App.vue中插入&lt;router-view&gt;

实现一个简单的路由

[https://gitee.com/chengbenchao/vue-pro](https://gitee.com/chengbenchao/vue-pro)

向项目克隆下来后,在命令行中安装依赖的npm包

```
npm i
```



