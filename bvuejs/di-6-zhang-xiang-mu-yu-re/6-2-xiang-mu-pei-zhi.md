1.引入reset.css

2.引入border.css

3.安装

//移动端会有事件延迟的问题,安装fastclick这个库

```
npm i fastclick --save
```

之后再main.js中引入

```
// 引入css文件
import '@/assets/styles/reset.css'
// 移动端1px边框的问题
import '@/assets/styles/border.css'
import fastClick from 'fastclick'

fastClick.attach(document.body)
```



