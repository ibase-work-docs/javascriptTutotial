#### CLI 会被全局安装并在终端里提供`vue`命令：

```
npm i vue-cli -g
npm i webpack -g
//创建一个基于webpack的新项目
npm init webpack my-project
cd my-project
npm i
npm run dev
```

[https://segmentfault.com/q/1010000007130348](https://segmentfault.com/q/1010000007130348)

#### npm install 安装模块或插件的时候，有两种命令把他们写入到 package.json 文件里面去

```
--save --dev
--save
```

--save-dev 安装的 插件，被写入到 devDependencies 对象里面去

--save 安装的插件，责被写入到 dependencies 对象里面去。

devDependencies  里面的插件只用于**开发环境**，不用于**生产环境**

dependencies  是需要发布到**生产环境**的。

