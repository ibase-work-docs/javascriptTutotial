项目结构

```
pages
    home
        components--Header.vue
        Home.vue
```

![](/assets/home.png)

技术要点：iconfont

更改别名在build——webpack.base.config.js

```
 resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': resolve('src'),
      //给('src/assets/styles')路径起个别名
      'styles' : resolve('src/assets/styles'),
    }
  },
```

https://gitee.com/ibase-work-docs/vue-project/tree/master/vue-pro-head

