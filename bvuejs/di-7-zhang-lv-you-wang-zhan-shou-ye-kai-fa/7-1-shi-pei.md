vscode安装环境

在vue中实现视配

1.安装lib-flexible

```
npm i lib-flexible --save
```

2.在main.js引入lib-flexible

```
import 'lib-flexible'
```

3.安装px2rem-loader

```
npm install px2rem-loader --save-dev
```

4.配置px2rem-loader

修改`build/utils.js`, 在cssLoader变量中

```js
  const cssLoader = {
    loader: 'css-loader',
    options: {
      sourceMap: options.sourceMap,
      importLoader: 5 // 在加载cssLoader之前加载的loader个数
    }
  }
  const px2remLoader = {
    loader: 'px2rem-loader',
    options: {
      emUnit: 75 // 设计稿的1/10
    }
  }
  // 在后面的函数中
  function generateLoaders(loader, loaderOptions) {
    const loaders = options.usePostCSS ? [cssLoader, postcssLoader, px2remLoader] : [cssLoader, px2remLoader]
    if (loader) {
      loaders.push({
        loader: loader + '-loader',
        options: Object.assign({}, loaderOptions, {
          sourceMap: options.sourceMap
        })
      })
    }
```

[http://www.yuanjingzhuang.com/2018/03/28/VUE中使用lib-flexible/](http://www.yuanjingzhuang.com/2018/03/28/VUE中使用lib-flexible/)

打包,采坑

[https://www.jianshu.com/p/ec0ce1f644d2](https://www.jianshu.com/p/ec0ce1f644d2)

