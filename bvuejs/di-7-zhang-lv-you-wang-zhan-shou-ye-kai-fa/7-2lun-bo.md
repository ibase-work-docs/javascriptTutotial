[https://github.com/surmon-china/vue-awesome-swiper](https://github.com/surmon-china/vue-awesome-swiper)

```
npm install vue-awesome-swiper@2.6.7 --save
```

[https://www.npmjs.com/package/vue-awesome-swiper](https://www.npmjs.com/package/vue-awesome-swiper)

在home-components下创建Swiper.vue

```js
<template>
  <div class="wrapper">
    <swiper :options="swiperOption" >
    <!-- slides -->
    <swiper-slide v-for="item of swiperList" :key="item.id">
      <img class="swiper-img"  :src="item.url">
      </swiper-slide>
    <!-- Optional controls -->
    <div class="swiper-pagination"  slot="pagination"></div>
  </swiper>
  </div>
</template>
<script>
export default {
  data () {
    return {
      swiperOption: {
        pagination: '.swiper-pagination',
        //循环播放
        loop: true,
        //自动播放的事件
        autoplay: 2000,
        //触摸时自动播放停止
        autoplayDisableOnInteraction: false
      },
      swiperList: [
        {
          id: '001',
          url:
            'https://dimg04.c-ctrip.com/images/700m0r000000haf6z6D9D_1242_470_191.jpg'
        },
        {
          id: '002',
          url:
            'https://dimg04.c-ctrip.com/images/70040s000000i78om5E8F_480_182_191.jpg'
        },
        {
          id: '003',
          url:
            'https://dimg04.c-ctrip.com/images/700r0s000000hw8l0CA21_1242_470_191.jpg'
        }
      ]
    }
  }
}
</script>
<style scoped>
.swiper-img {
  width: 100%;
}
.wrapper {
  background: #eee;
}
//设置小圆点
.wrapper >>> .swiper-pagination-bullet-active {
  background: #fff !important;
}
</style>
```

[https://gitee.com/ibase-work-docs/vue-project/tree/master/vue-pro-swiper](https://gitee.com/ibase-work-docs/vue-project/tree/master/vue-pro-swiper)

![](/assets/swiper.gif)

