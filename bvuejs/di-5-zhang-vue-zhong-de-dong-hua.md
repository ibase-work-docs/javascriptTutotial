![](/assets/fadeEnter.jpg)![](/assets/fadeLeave.jpg)

```
//显示时
.fade-enter
//显示的过程中
.fade-enter-active
```

```
//隐藏
.fade-leave-to
//隐藏的过程中
.fade-leave-active
```





```js
    <div id="app">
        <transition>
            <div v-show="show">hello world</div>
        </transition>

        <button @click="handleClick">切换</button>
    </div>
    <script>
        var app = new Vue({
            el: "#app",
            data: {
                show: true
            },
            methods: {
                handleClick: function () {
                    this.show = !this.show;
                }
            }
        })
    </script>
```

```js
//动态添加css样式
        //显示时
        .v-enter{
            opacity:0;
        }
        //显示的过程中一直存在
        .v-enter-active{
            transition:opacity 1s;
        }
        //隐藏
        .v-leave-to{
            opacity: 0;
        }
        //隐藏的过程中一直存在
        .v-leave-active{
            transition:opacity 1s;
        }
```



