## [绑定 HTML Class](https://cn.vuejs.org/v2/guide/class-and-style.html#绑定-HTML-Class) {#绑定-HTML-Class}

## 1.对象语法 {#绑定-HTML-Class}

```
    <div id="app">
            <div class="test" v-bind:class="{ active: isActive }"></div>
    </div>
    <script>
        new Vue({
            el:"#app",
            data:{
                isActive:true
            }
        })
    </script>
```

结果渲染为:

```
<div class="test active"></div>
```

Tip:绑定的数据对象不必内联定义在模板里

```
<div v-bind:class="classObj"></div>
data:{
    classObj:{
        active:true,
        text-danger:false
    }
}
```

结果渲染为:

```
<div class="active"></div>
```

## 2.数组

```
<div  v-bind:class="[firstClass,secondClass]"></div>
```

```
data:{
    firstClass:'first',
    secondClass:'second'
}
```



