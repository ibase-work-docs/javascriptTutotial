#### 1.属性的绑定——属性绑定data数据

```js
    <div id="root">
        <!-- v-bind:title表示title这个属性和data里的title绑定 -->
        //<div v-bind:title="title">hello world</div>
        <div :title="title">hello world</div>
    </div>
    <script>
        new Vue({
            el:"#root",
            data:{
                title:"this is hell0"
            }
        })
    </script>
```

Tip：v-bind:title="title"和数据项data里的title绑定  v-bind:title="";双冒号里的是js表达式

```
<div v-bind:title="'good'+title">hello world</div>
```

Tip:当使用模板指定v-\*,双冒号里的内容就是js的表达式,v-bind:title可以简写为:title

#### 2.双向数据绑定

```js
    <div id="root">
        <!-- v-model这个模板指令表示双向数据绑定 -->
        <input type="text" v-model="content">
        <div>{{content}}</div>
        
    </div>
    <script>
        new Vue({
            el:"#root",
            data:{
                content:"hello world"
            }
        })
    </script>
```





