#### 1.v-if  //将标签从DOM上移除

//控制DOM的存在与否

```js
    <div id="root">
        <div v-if="show">hello world</div>
        <button @click="handleClick">toggle</button>
    </div>
    <script>
        new Vue({
            el:"#root",
            data : {
                show:false
            },
            methods : {
                handleClick : function(){
                   this.show=!this.show;
                }
            }
        })
    </script>
```

#### 2.v-show //display:none

//控制DOM的显示与否

```js
    <div id="root">
        <div v-show="show">hello world</div>
        <button @click="handleClick">toggle</button>
    </div>
    <script>
        new Vue({
            el:"#root",
            data : {
                show:true
            },
            methods : {
                handleClick : function(){
                   this.show=!this.show;
                }
            }
        })
    </script>
```

#### 3.v-for

//当有数据要作循环展示时,就用v-for

```js
    <div id="root">
        <ul v-for="item of lists">
            <li>{{item}}</li>
        </ul>
    </div>
    <script>
        new Vue({
            el:'#root',
            data:{
                lists:[1,2,3]
            }
        })
    </script>
```

提高渲染性能加 :key这个属性

```js
    <div id="root">
        <ul >
        //index表示下标
            <li v-for"(item,index) of list" :key="index">{{item}}</li>
        </ul>
    </div>
    <script>
        new Vue({
            el:'#root',
            data:{
                lists:[1,2,3]
            }
        })
    </script>
```



