#### 1.data的数据显示在模板上** **`v-text/html`

v-text和v-html的区别

会输出

```js
    <div id="root">
      <h1 v-text="content"></h1>
    </div>
    <script>
        new Vue({
            el:"#root",
            // template:"<div>{{msg}}</div>",
            data:{
                msg:"hello world",
                num:"123",
                content:"<h1>content</h>"
            }
        })
    </script>
```

```
<h1>content</h>
```

Tip:v-text会将数据作用文本输出

```js
    <div id="root">
      <h1 v-html="content"></h1>
    </div>
    <script>
        new Vue({
            el:"#root",
            // template:"<div>{{msg}}</div>",
            data:{
                msg:"hello world",
                num:"123",
                content:"<h1>content</h>"
            }
        })
    </script>
```

输出：  
![](/assets/centent.png)

Tip:v-html将内容作用html输出

#### 2.给模板上的标签定义一个事件 `v-on:event="eventName"`

```js
    <div id="root">
        <!-- 触发的事件定义的方法,放在实例中的methods方法中 -->
        //v-on:click可以简写为@click
      //<div v-on:click="handleClick">{{content}}</div>
      <div @click="handleClick">{{content}}</div>

    </div>
    <script>
        new Vue({
            el:"#root",
            // template:"<div>{{msg}}</div>",
            data:{
                content:"content"
            },
            methods:{
                handleClick:function(){
                //this.content指vue实例下的data下的content
                    this.content="hello world"
                }
            }
        })
    </script>
```

1.为元素执行点击事件的元素绑定v-on:click="handleClick"

2.将方法放在实例的methods属性中

Tip：v-on:click可以简写为@click,后面的代码都会用@click

