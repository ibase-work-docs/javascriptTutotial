挂载点,模板,实例

#### 1.模板写在挂载点中

```js
    <div id="root">
    //挂载点里的内容为模板
       {{msg}}
    </div>
    <script>
    //实例
        new Vue({
        //挂载点
            el:"#root",
            data:{
                msg:"hello world"
            }
        })
    </script>
```

* 实例只会渲染挂载点
* 挂载点里的内容为模板

#### 2.模板写在实例中

```js
    <div id="root">

    </div>
    <script>
        new Vue({
            el:"#root",
            template:"<div>{{msg}}</div>",
            data:{
                msg:"hello world"
            }
        })
    </script>
```



