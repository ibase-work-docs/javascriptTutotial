#### 1.computed计算属性

```js
    <div id="root">
        <input type="text" v-model="firstName">
        <input type="text" v-model="lastName">
        <div>{{fullName}}</div>
    </div>
    <script>
        new Vue({
            el:"#root",
            data:{
                firstName:'',
                lastName:''
            },
            computed:{
                fullName:function(){
                    return this.firstName+" "+this.lastName;
                }
            }
        })
    </script>
```

#### 2.watch——侦听器

原理:侦听某个数据的变化,一旦它变化就可以做业务逻辑

eg:firstName或lastName任意变动,count加一

```js
    <!-- 对姓名作任意变更,count加一 -->
    <div id="root">
        <input type="text" v-model="firstName">
        <input type="text" v-model="lastName">
        <div>{{fullName}}</div>
        <div>{{count}}</div>
    </div>
    <script>
        new Vue({
            el:"#root",
            data:{
                firstName:'',
                lastName:'',
                count:0
            },
            computed:{
                fullName:function(){
                    return this.firstName+" "+this.lastName;
                }
            },
            watch:{
                firstName:function(){
                    this.count++;
                },
                lastName:function(){
                    this.count++;
                }
            }
        })
    </script>
```

```js
            watch:{
                fullName : function(){
                    this.count++;
                }
            }
```



