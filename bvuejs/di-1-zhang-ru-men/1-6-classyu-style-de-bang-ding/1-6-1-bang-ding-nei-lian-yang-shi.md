### 1.对象语法

```html
<div  v-bind:style='{color:active}'>hello world</div>
```

```js
data:{
    active:"red"
}
```

直接绑定到一个[样式对象](/0)通常更好，这会让模板更清晰：

```html
 <div  v-bind:style='styleObj'>hello world</div>
```

```
data:{
    styleObj:{
        color:"green",
        fontSize:"20px"
    }
}
```

## 2.数组语法

将[多个样式对象](/1)应用到同一个元素上

```
<div v-bind:style="[firstStyle,secondStyle]"></div>
```

```js
    <div id="app">
                <div  v-bind:style='[firstStyle,secondStyle]' @click="handleClick">hello world</div>
        </div>
        <script>
            new Vue({
                el:"#app",
                data:{
                    firstStyle:{
                        color:"green",
                        fontSize:"20px"
                    },
                    secondStyle:{
                        background:"red"
                    }
                },
                methods:{
                    handleClick:function(){
                        this.first.color=(this.first.color==="green")?"black":"green"
                    }
                }
            })
```

//加一个点击事件

