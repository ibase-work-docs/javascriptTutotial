App.vue

```js
<template>
   <div>
     <input type="text" v-model="inputValue">
     <button @click="handleSubmit">提交</button>
     <ul>
       <todo-item v-for="(item,index) of list" :key='index' 
       :content="item" :index="index" @delete='handleDelete'></todo-item>
     </ul>
   </div>
</template>
<script>
import TodoItem from "./components/TodoItem";
export default {
  components: {
    "todo-item": TodoItem
  },
  data: function() {
    return {
      inputValue: "",
      list: []
    };
  },
  methods: {
    handleSubmit: function() {
      this.list.push(this.inputValue);
      console.log(this.list);
    },
    handleDelete (index){
      this.list.splice(index,1);
    }
  }
};
</script>
```

TodoItem.vue

```js
<template>
    <li @click="handleClick">{{content}}</li>
</template>

<script>
export default {
  props: ["content", "index"],
  methods: {
    handleClick: function() {
      this.$emit("delete", this.index);
    }
  }
};
</script>
```

使用组件

* 1.import引入组件
* 2.components注册

TodoItem.vue

```
<style scoped>
//样式的作用范围,仅仅只在这个组件
</style>
```



