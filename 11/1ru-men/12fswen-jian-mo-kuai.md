文件操作:fs——file system

```js
const fs = require("fs");
//readFile(fileName,func)
fs.readFile();
//writeFile(fileName,content,func)
fs.writeFile();
```

nodejs是异步操作;

#### 1.读取文件 `readFile(fileName,func)`

```js
const fs = require("fs");
fs.readFile("a.txt",function(err,data){
    if(err){
        console.log("读取失败");
    }else{
        console.log(data.toString());
    }
})
```

#### 2.写文件`writeFile(fileName,content,func)`

```js
const fs =  require("fs");
fs.writeFile("b.txt","hello world",function(err){
    console.log(err);
})
```

#### 3.结合server的读取文件

```js
const http =  require("http");
const fs = require("fs");
http.createServer(function(req,res){
    var fileName = "./www"+req.url;
    fs.readFile(fileName,function(err,data){
        if(err){
            res.write("404")
        }else{
            res.write(data)
        }
        res.end();
    })
})
```

#### 在线实例

https://gitee.com/frontendLol/nodeTutorial/tree/master/fs02

