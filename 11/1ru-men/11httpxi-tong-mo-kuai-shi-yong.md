http——协议

req——请求

res——相应

### 1.实现一个简单的服务器

```js
const http = require("http");
var server = http.createServer(function(res,req){

    res.write("hello world");
    res.end();
})
server.listen(8080);
```

#### createServer创建一个服务器

#### res.write\(\)向前台输出东西;

#### res.end\(\)结束请求

### 2.根据[req.url](/req.url)的请求,动态相应对于的页面

```js
const http = require("http");
var server = http.createServer(function(res,req){
    switch(req.url){
        case:"/1.html":
        res.write("111");
        break;
        case:"/2.html":
        res.write("222");
        break;
        default:
        res.write("404");
        break;
    }
    res.end();
})
server.listen(8080);
```

### 在线demo地址：

https://gitee.com/frontendLol/nodeTutorial/tree/master/http01

